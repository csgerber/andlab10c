package edu.uchicago.gerber.andlab10c;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by agerber on 5/27/2016.
 */
public class CrowdWeather extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }
}
