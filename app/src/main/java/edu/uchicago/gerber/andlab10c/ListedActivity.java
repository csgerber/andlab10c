package edu.uchicago.gerber.andlab10c;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.ui.FirebaseListAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ListedActivity extends AppCompatActivity {

    private ListView lvContacts;
    private Firebase dbReference;
    private Button addRandomMovie;
    private ArrayList<Movie> mMovies;
    private Random mRandom;
    private FirebaseListAdapter<Movie> mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listed);
        mRandom = new Random();
        mMovies = new ArrayList<>();
        mMovies.add(

                new Movie("Money Monster",
                        "http://cdn2.thr.com/sites/default/files/imagecache/landscape_928x523/2016/01/money_monster_trailer_still.jpg"));
        mMovies.add(
                new Movie("Fury Road",
                        "https://i.ytimg.com/vi/hEJnMQG9ev8/maxresdefault.jpg"));

        mMovies.add(
                new Movie("Apocolypse Now",
                        "http://empireonline.media/jpg/80/0/0/1000/563/0/north/0/0/0/0/0/t/films/28/images/k91Dag8AZbhjIJqrtf7F1bQnGPg.jpg"));

        mMovies.add(

                new Movie("American Sniper",
                        "https://i.ytimg.com/vi/99k3u9ay1gs/maxresdefault.jpg"));


        addRandomMovie = (Button) findViewById(R.id.addRandomMovie);
        addRandomMovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dbReference.child("Movie").push().setValue(mMovies.get(mRandom.nextInt(mMovies.size())));


            }
        });

//        removeRandomMovie = (Button) findViewById(R.id.removeRandomMovie);
//        removeRandomMovie.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//             String strName =     mMovies.get(mRandom.nextInt(mMovies.size())).getName();//some random name
//
//                dbReference
//                        .child("Movie") //the Movie Branch
//                        .child(strName) // the key of the object
//                        .child("name") //actually using "color" because that is the name of the key. This will remove the value, and because there are no more values, the key is empty, therefore it will be deleted too.
//                        .removeValue();  // delete
//
//              //  dbReference.child("Movie").push().setValue(mMovies.get(mRandom.nextInt(mMovies.size())));
//            }
//        });

        dbReference = new Firebase("https://andlab10c.firebaseio.com");
        Firebase messagesRef = dbReference.child("Movie");
        messagesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                refreshData();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        lvContacts = (ListView) findViewById(R.id.lvContacts);




    }

    @Override
    protected void onStart() {
        super.onStart();

        refreshData();

    }

    private void refreshData() {
        Firebase messagesRef = dbReference.child("Movie");
        mAdapter = new FirebaseListAdapter<Movie>(

                this,
                Movie.class,
                 R.layout.restos_row,
                messagesRef



        )
        {
            @Override
            protected void populateView(View view, Movie s, int i) {


                View list_tab = view.findViewById(R.id.list_tab);
                if (s.isFav())
                   list_tab.setBackgroundResource(R.color.orange);
                else
                    list_tab.setBackgroundResource(R.color.green);

                TextView textView = (TextView) view.findViewById(R.id.list_text);
                ImageView img = (ImageView) view.findViewById(R.id.list_niv);
                textView.setText(s.getName());
                Glide.with(getApplicationContext())
                        .load(s.getUrl())
                        .placeholder(R.drawable.ic_google)
                        .into(img);
            }


        };




        lvContacts.setAdapter(mAdapter);

        lvContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movie movie = (Movie) lvContacts.getItemAtPosition(position);
                Firebase itemRef = mAdapter.getRef(position);
                itemRef.child("fav").setValue(!movie.isFav());

            }
        });

        lvContacts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                Movie movie = (Movie) lvContacts.getItemAtPosition(position);
                Map episodes = movie.getEpisodes();
                if (episodes != null) {
                    StringBuilder stringBuilder = new StringBuilder();
                    List<String> seasonEps = (List<String>) episodes.get("Season1");

                    for (String str : seasonEps) {
                        stringBuilder.append(str + " : ");
                    }

                    Toast.makeText(ListedActivity.this, stringBuilder.toString(), Toast.LENGTH_SHORT).show();
                }


                Firebase itemRef = mAdapter.getRef(position);

                 itemRef.removeValue();

                return true;
            }
        });


    }
}
