package edu.uchicago.gerber.andlab10c;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    private Button buttonSunny, buttonFoggy;
    private TextView textViewCondition;
    private Firebase dbReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    @Override
    protected void onStart(){
        super.onStart();
        buttonFoggy = (Button) findViewById(R.id.buttonFoggy);
        buttonSunny = (Button) findViewById(R.id.buttonSunny);
        textViewCondition = (TextView) findViewById(R.id.textViewCondition);
        dbReference = new Firebase("https://andlab10c.firebaseio.com/condition");
        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String text = dataSnapshot.getValue(String.class);
                textViewCondition.setText(text);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        buttonFoggy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dbReference.setValue("Foggy");

            }
        });

        buttonSunny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dbReference.setValue("Sunny");
            }
        });

    }
}
