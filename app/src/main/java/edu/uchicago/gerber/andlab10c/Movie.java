package edu.uchicago.gerber.andlab10c;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Movie {

    private String name;
    private String url;
    private boolean fav;
    private Map<String, List<String>> episodes;



    public Movie() {
    }


    public boolean isFav() {
        return fav;
    }

    public void setFav(boolean fav) {
        this.fav = fav;
    }

    public Movie(String name, String url) {
        this.name = name;
        this.url = url;


        Random random = new Random();


        episodes = new HashMap<>();
        String[] strEpisodes = {"Season1", "Season2", "Season3", "Season4"};
        for (String ep : strEpisodes) {
        int nRanad = random.nextInt(10);
          List  scenes = new ArrayList<>();
            for (int nC = 0; nC < nRanad + 3; nC++) {
                scenes.add(name +" scene " + nC);
            }
            episodes.put(ep, scenes );
        }


        setFav(false);




  ;
    }

    public Map<String, List<String>> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(Map<String, List<String>> episodes) {
        this.episodes = episodes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
